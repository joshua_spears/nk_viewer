/* eslint-disable */
import api from '../../helpers/patient.hlpr'

const state = {
    vitalsigns: {},
    options: [],
    times: [],
    tableData: []
}


const getters = {
    vitalsigns: ({ vitalsigns }) => vitalsigns,
    options: ({ options }) => options,
    times: ({ times }) => times,
    tableData: ({ tableData }) => tableData
}


const actions = {
    async queryPatientVitals({ commit, getters, dispatch }) {
        const { patient, device, stayParams } = getters
        const { deviceKey } = device
        const { id } = patient
        try {
            const { data } = await api.getPatientVitals({
                patientID: id,
                deviceKey,
                start: stayParams.date,
                interval: stayParams.interval
            })

            const points = []
            for (let opt of Object.keys(data.vitalsigns)) {
                const vital = []
                for (let point of data.vitalsigns[opt]) {
                    vital.push({ y: parseInt(point.result), time: point.time })
                }
                points.push({
                    title: opt,
                    dataPoints: vital,
                    markerSize: 5,
                    type: 'scatter'
                })
            }
            commit('setVitalsigns', points)
            dispatch('assignTimes')

            dispatch('assignTableData')
        } catch (err) {
            console.error(err)
        }
    },

    async assignOptions ({ commit, dispatch }, options) {
        commit('setOptions', options)
        await dispatch('assignTimes')
        await dispatch('assignTableData')
    },

    async assignTimes ({ commit, getters }) {
        const { date, interval } = getters.stayParams
        var time = new Date(date)
        let times = []
        for (let i = 0; i < 12; ++i) {
            const t = time.getTime()
            const hours = (time.getHours() < 10 ? '0' : '') + time.getHours()
            const minutes = (time.getMinutes() < 10 ? '0' : '') + time.getMinutes()
            const seconds = (time.getSeconds() < 10 ? '0' : '') + time.getSeconds()
            times.push({ time: time.getTime(), title: hours + ':' + minutes + ':' + seconds })
            time.setTime(t + (interval * 60000))
        }
        commit('setTimes', times)
    },

    async assignTableData ({ commit, getters }) {
        const { vitalsigns, options, times } = getters
        if (!options.length) return

        const all = options.includes('all')
        const vitals = all ?
            vitalsigns :
            vitalsigns.filter(v => options.includes(v.title))

        // this is a brute force solution
        // we were meant to optimize later
        const rows = []
        for (const v of vitals) {
            const row = {
                title: v.title,
                data: []
            }
            for (const t of times) {
                for (const p of v.dataPoints) {
                    const pointDate = new Date(p.time)
                    const diff = Math.abs(pointDate.getTime() - t.time)
                    if (diff <= 1020) {
                        row.data.push(p.y)
                        break
                    }
                }
            }
            rows.push(row)
        }
        commit('setTableData', rows)
    }
}


const mutations = {
    setVitalsigns: (state, vitals) => (state.vitalsigns = vitals),
    setOptions: (state, options) => (state.options = options),
    setTimes: (state, times) => (state.times = times),
    setTableData: (state, tableData) => (state.tableData = tableData)
}


export default {
    state,
    getters,
    actions,
    mutations
}
