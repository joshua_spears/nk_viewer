/* eslint-disable */
import Vue from 'vue'
import Vuex from 'vuex'

import patients from './modules/patients.js'
import stay from './modules/stay.js'
import vitalsigns from './modules/vitalsigns.js'
import waveforms from './modules/waveforms.js'

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        patients,
        stay,
        vitalsigns,
        waveforms
    }
})
