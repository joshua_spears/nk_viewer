/* eslint-disable */
import SearchForm from "../../components/SearchFrom/SearchForm.vue"
import Loader from '../../components/Loader/Loader.vue'

import { mapGetters, mapActions } from "vuex"
import M from "materialize-css"

const actions = [ 'queryPatients', 'queryPatientStay', 'selectPatient', 'selectDevice', 'assignStayParams', 'assignStayInfo' ]
const getters = [ 'patients', 'filteredPatients', 'patient', 'stay', 'stayInfo', 'stayParams', 'device' ]

export default {
    name: 'Select',

    components: {
        SearchForm,
        Loader
    },

    mounted () {
        M.AutoInit()
        // DATEPICKER OPTIONS
        const options = {
            yearRange: [
                new Date().getFullYear() - 120,
                new Date().getFullYear()
            ]
        }
        // SETTING THE YEAR RANGE FOR THE DATEPICKER
        const datepicker = document.querySelectorAll('.datepicker')
        M.Datepicker.init(datepicker, options)
    },

    async created () {
        this.initializePage()
    },

    computed: mapGetters(getters),

    methods: {
        ...mapActions(actions),

        async initializePage () {
            await this.queryPatients()
        },

        async onPatientSelect (e) {
            const patient = JSON.parse(e.target.attributes.patient.value)
            const [ device ] = patient.devices
            await this.selectPatient(patient)
            await this.selectDevice(device.deviceID)
            await this.queryPatientStay()
        },

        onTypeSelect (e) {
            const type = e.target.attributes.typeName.value
            const { date, interval, duration } = this.stayParams
            this.assignStayParams({
                date,
                interval,
                duration,
                type
            })
        },

        patientDate (e) {
            const newDate = e.target.value
            const { interval, duration, type, date } = this.stayParams
            const newFormattedDate = newDate.split('').splice(0, 10).join('') + date.split('').splice(10, date.length).join('')
            this.assignStayParams({
                interval,
                duration,
                date: newFormattedDate,
                type
            })
        },

        deviceSelect (e) {
            const name = e.target.value
            this.selectDevice(name)
        },

        onIntervalInput (e) {
            const interval = e.target.value
            const { type, chosenDate, duration, date } = this.stayParams
            this.assignStayParams({
                chosenDate,
                interval,
                duration,
                date,
                type
            })
        },


        onStartChange (e) {
            const newTime = e.target.value
            const { interval, duration, type, date } = this.stayParams
            // const newFormattedDate = newDate.split('').splice(0, 10).join('') + date.split('').splice(10, date.length).join('')
            // this.assignStayParams({
            //     interval,
            //     duration,
            //     date: newFormattedDate,
            //     type
            // })
        }
    }
}
