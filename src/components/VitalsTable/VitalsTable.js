/* eslint-disable */
import { mapGetters } from "vuex"

export default {
    name: 'VitalsTable',

    computed: mapGetters([ 'times', 'tableData' ]),
}
