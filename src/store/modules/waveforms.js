/* eslint-disable */
import api from '../../helpers/patient.hlpr'

const state = {
    waveforms: {}
}


const getters = {
    waveforms: state => state.waveforms
}


const actions = {
    async queryPatientWaveforms({ commit, getters }) {
        const { device, patient, stayParams } = getters

        try {
            const { data } = await api.getPatientWaveforms({
                patientID: patient.id,
                deviceKey: device.deviceKey,
                date: new Date(stayParams.date).toISOString()
            })

            commit('setWaveforms', data.waveforms)
        } catch (err) {
            console.error(err)
        }
    }
}


const mutations = {
    setWaveforms: (state, waveforms) => (state.waveforms = waveforms)
}


export default {
    state,
    getters,
    actions,
    mutations
}
