/* eslint-disable */
import LineChart from '../../components/LineChart/LineChart.vue'
import Loader from "../../components/Loader/Loader.vue";

import { mapGetters, mapActions } from "vuex"
import M from "materialize-css"


export default {
    name: 'Waveforms',

    components: {
        LineChart,
        Loader
    },

    data () {
        return {
            pageOptions: [],
            points: [],
            duration: ''
        }
    },

    computed: mapGetters([ 'waveforms' ]),

    async mounted () {
        M.AutoInit()
        if (!this.waveforms.length) {
            try {
                await this.queryPatientWaveforms()
                M.AutoInit()
                console.log(this.waveforms)
            } catch (err) {
                console.error(err)
            }
        }
    },

    methods: {
        ...mapActions([ 'queryPatientWaveforms' ]),

        onWaveformInput () {
            console.log(this.waveforms)
            // this.points = []
            // for (let opt of this.options) {
            //     const wavePoints = []
            //     for (let point of this.waveforms[opt]) {
            //         wavePoints.push({ y: parseInt(point.RESULT) })
            //     }
            //
            //     this.points.push({ title: opt, data: wavePoints })
            // }
        }
    }
}
