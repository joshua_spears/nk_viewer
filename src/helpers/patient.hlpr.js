/* eslint-disable */
import axios from 'axios'

let ROUTE
if (process.env.NODE_ENV === 'production') {
    ROUTE = process.env.VUE_APP_END_POINT + '/api/patient'
}
else ROUTE = '/api/patient'


export default {
    getPatientList () {
        return axios.get(`${ROUTE}/list`)
    },

    getPatientStay (params) {
        const options = { params }
        return axios.get(`${ROUTE}/stay`, options)
    },

    getPatientVitals (params) {
        const options = { params }
        return axios.get(`${ROUTE}/vitals`, options)
    },

    getPatientWaveforms (params) {
        const options = { params }
        return axios.get(`${ROUTE}/waveforms`, options)
    }
}
