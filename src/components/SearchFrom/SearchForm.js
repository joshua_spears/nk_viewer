/* eslint-disable */
import { mapActions, mapGetters } from "vuex"


export default {
    name: "SearchForm",

    data () {
        return {
            search: {
                firstName: '',
                lastName: '',
                mrn: '',
                deviceId: ''
            }
        }
    },

    methods: {
        ...mapActions(['filterPatients']),

        onSearch (e) {
            this.filterPatients(this.search)
        }
    },

    computed: mapGetters(['filteredPatients'])
}
