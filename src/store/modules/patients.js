/* eslint-disable */
import api from '../../helpers/patient.hlpr'

const state = {
    patients: [],
    filteredPatients: [],
    patient: {}
}


const getters = {
    patients: ({ patients }) => patients,
    filteredPatients: ({ filteredPatients }) => filteredPatients,
    patient: ({ patient }) => patient
}


const actions = {
    async queryPatients ({ commit, dispatch }) {
        try {
            const { data } = await api.getPatientList()
            const { patients } = data
            const [ patient ] = patients
            const [ device ] = patient.devices
            commit('setPatients', patients)
            commit('setFilteredPatients', patients)
            commit('setPatient', patient)
            dispatch('selectDevice', device.deviceID)
            await dispatch('queryPatientStay')
        } catch (err) {
            console.error(err)
        }
    },


    async selectPatient ({ commit, getters, dispatch }, patient) {
        const { patients } = getters
        const [ p ] = patients
            .filter(pat =>
                pat.patientID === patient.patientID &&
                pat.patientKey === patient.patientKey
            )
        commit('setPatient', p)

        const [ device ] = p.devices
        dispatch('selectDevice', device.name)
    },


    filterPatients ({ commit, getters, dispatch }, search) {
        const datepicker = document.getElementById('dob')
        let dob

        // ERROR HANDLING FOR DATE PICKER
        if (datepicker.M_Datepicker.date) dob = datepicker.M_Datepicker.date.toISOString().slice(0, 10)
        else dob = ''

        const filteredList = getters.patients.filter(patient =>
            patient.info.firstName.toLowerCase() === search.firstName.toLowerCase() ||
            patient.info.lastName.toLowerCase() === search.lastName.toLowerCase() ||
            patient.info.patientId.toLowerCase() === search.mrn.toLowerCase() ||
            patient.devices.filter(device => device.deviceId === search.deviceId).length > 0 ||
            new Date(patient.info.dob).toISOString().slice(0, 10) === dob
        )

        // IF FILTEREDLIST COMES UP WITH NOTHING FOUND - RESET THE LIST
        if (!filteredList.length) dispatch('resetSelectedPatient')
        else dispatch('assignSelectedPatient', filteredList)
    },

    resetSelectedPatient ({ getters, commit, dispatch }) {
        const patients = getters.patients
        const [ patient ] = patients
        commit('setFilteredPatients', patients)
        commit('setSelectedPatient', patient)
        dispatch('queryPatientStay')
    },

    assignPatient ({ getters, commit, dispatch }, filteredList) {
        const [ patient ] = filteredList
        commit('setFilteredPatients', filteredList)
        commit('setPatient', patient)
        dispatch('queryPatientStay')
    }
}


const mutations = {
    setPatients: (state, patients) => (state.patients = patients),
    setPatient: (state, patient) => (state.patient = patient),
    setFilteredPatients: (state, filteredPatients) => (state.filteredPatients = filteredPatients)
}


export default {
    state,
    getters,
    actions,
    mutations
}
