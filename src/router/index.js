/* eslint-disable */
import Vue from 'vue'
import VueRouter from 'vue-router'

import Select from '../views/Select/Select.vue'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Select',
        component: Select
    },
    {
        path: '/vitalsigns',
        name: 'Vitalsigns',
        component: () => import('../views/Vitals/Vitals.vue')
    },
    {
        path: '/waveforms',
        name: 'Waveforms',
        component: () => import('../views/Waveforms/Waveforms.vue')
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router
