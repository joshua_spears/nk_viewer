/* eslint-disable */
import api from '../../helpers/patient.hlpr'

const state = {
    stay: {
        info: {},
        params: {}
    },
    device: {}
}


const getters = {
    stay: ({ stay }) => stay,
    stayInfo: ({ stay }) => stay.info,
    stayParams: ({ stay }) => stay.params,
    device: ({ device }) => device
}


const actions = {
    async queryPatientStay ({ commit, getters, dispatch }) {
        try {
            const { device } = getters
            const { data } = await api.getPatientStay(device)

            const { types, range } = data.stay
            const type = types.find(t => t.name === 'Vitalsign')
            const date = range.find(d => d.type === 'Vitalsign')
            const params = {
                date: date.min,
                duration: 60,
                interval: 5,
                type: type.name
            }

            data.stay.displayDates = data.stay.dates.reduce((arr, d) => {
                if (arr.includes(d.displayDate)) return arr
                return [ ...arr, d.displayDate]
            }, [])

            commit('setInfo', data.stay)
            commit('setParams', params)

            await dispatch('selectDevice', device.deviceID)
        } catch (err) {
            console.error(err)
        }
    },

    async selectDevice ({ commit, getters }, name) {
        const { patient } = getters
        const { devices } = patient
        const device = devices.find(d => d.deviceID === name)
        commit('setDevice', device)
    },

    assignStayParams ({ commit, getters }, params) {
        commit('setParams', params)
    },

    assignStayInfo ({ commit, getters }, info) {
        commit('setInfo', info)
    }
}


const mutations = {
    setInfo: (state, info) => (state.stay.info = info),
    setParams: (state, params) => (state.stay.params = params),
    setDevice: (state, device) => (state.device = device)
}


export default {
    state,
    getters,
    actions,
    mutations
}
