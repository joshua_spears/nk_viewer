/* eslint-disable */
import {mapGetters} from "vuex";

const CanvasJS = window.CanvasJS

export default {
    name: 'PlotChart',

    data () {
        return {
            chartOptions: {
                zoomEnabled: true,
                zoomType: "x",
                title: {
                    text: this.title,
                    fontSize: 25,
                    // fontColor: "#fff",
                    fontWeight: 'lighter'
                },
                legend: {
                    horizontalAlign: "right",
                    verticalAlign: "center"
                },
                axisY: {
                    includeZero: false,
                    // gridColor: "#fff",
                    // labelFontColor: "#fff"
                },
                // axisX:{
                //     viewportMinimum: 0,
                //     viewportMaximum: 500
                // }
            },
            chart: null
        }
    },

    mounted () {
        const all = this.options.includes('all')
        // IF ALL IS IN OPTIONS THEN CHART ALL VITALSIGNS ELSE FILTER THE OPTIONS
        const data = all ? this.vitalsigns : this.vitalsigns.filter(v => this.options.includes(v.title))

        this.chartOptions.data = data

        this.chart = new CanvasJS.Chart(`chartContainer`, this.chartOptions)
        this.chart.render()
    },

    computed: mapGetters([ 'vitalsigns', 'options' ])
}
