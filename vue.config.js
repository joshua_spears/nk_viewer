module.exports = {
    runtimeCompiler: true,
    devServer: {
        // proxy: 'http://10.0.104.10:3000'
        proxy: 'http://127.0.0.1:3000'
    },
    transpileDependencies: [
        'canvasjs'
    ]
}
