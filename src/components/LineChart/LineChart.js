/* eslint-disable */
import {mapGetters} from "vuex";

const CanvasJS = window.CanvasJS

export default {
    name: 'LineChart',

    props: {
        chartData: Array,
        title: String,
        index: Number
    },

    computed: mapGetters([ 'waveforms' ]),

    data () {
        return {
            chartOptions: {
                zoomEnabled: true,
                title: {
                    text: this.title,
                    fontSize: 25,
                    // fontColor: "#fff",
                    fontWeight: 'lighter'
                },
                axisY: {
                    includeZero: false,
                    // gridColor: "#fff",
                    // labelFontColor: "#fff"
                },
                axisX: {
                    gridThickness: 0,
                    // gridColor: "#fff",
                    tickLength: 0,
                    lineThickness: 0,
                    labelFormatter: function () {
                        return ' '
                    }
                },
                data: [
                    {
                        type: 'line',
                        lineColor: '#d50000 ',
                        markerType: "none",
                        dataPoints: []
                    }
                ]
            },
            chart: null
        }
    },

    mounted () {
        this.chartOptions.data[0].dataPoints = this.chartData

        console.log(this.chartData)
        this.chart = new CanvasJS.Chart(`chartContainer-${this.index}`, this.chartOptions)
        this.chart.render()
    }
}
