/* eslint-disable */
import PlotChart from '../../components/PlotChart/PlotChart.vue'
import Loader from "../../components/Loader/Loader.vue"
import VitalsTable from "../../components/VitalsTable/VitalsTable.vue"

import { mapGetters, mapActions } from "vuex"
import M from "materialize-css";


export default {
    name: 'Vitals',

    components: {
        PlotChart,
        VitalsTable,
        Loader
    },

    data () {
        return {
            pageOptions: [],
            pageInterval: 0
        }
    },

    async mounted () {
        await this.remountComponent()
    },

    methods: {
        ...mapActions([ 'queryPatientVitals', 'assignStayParams', 'assignOptions', 'assignTimes', 'assignStayParams', 'assignOptions' ]),

        async remountComponent () {
            await this.queryPatientVitals()
            M.AutoInit()
        },

        async onVitalsignInput () {
            await this.assignOptions(this.pageOptions)
        },

        async onIntervalInput () {
            const { date, type, duration } = this.stayParams
            this.assignStayParams({
                interval: this.pageInterval,
                duration,
                date,
                type
            })
            this.assignTimes()
            await this.remountComponent()
        },

        async changeTime (e) {
            const { date, interval, type, chosenDate, duration } = this.stayParams
            const value = e.target.attributes.value.value
            const start = new Date(date)
            const minutes = (interval * 12) * 60000
            let newDate
            if (value === '+') newDate = new Date(start.getTime() + minutes)
            else newDate = new Date(start.getTime() - minutes)
            this.assignStayParams({
                chosenDate,
                interval,
                duration,
                date: newDate.toISOString(),
                type
            })

            this.remountComponent()
        }
    },

    computed: mapGetters([ 'vitalsigns', 'device', 'patient', 'stay', 'stayParams', 'options' ])
}
